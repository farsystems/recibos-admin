import { Injectable } from "@angular/core";
import { Title } from "@angular/platform-browser";
import * as fs from 'fs';
import { IpcService } from "./ipc.service";

@Injectable()
export class TitleService {
    private appTitle: string;
    constructor(private _title: Title, ipcService: IpcService) {
        this.appTitle = JSON.parse(ipcService.sendSync('getApplicationPackage')).productName;
    }

    getTitle(): string {
        return this._title.getTitle();
    }

    setTitle(newTitle: string) {
        this._title.setTitle(this.appTitle + ' - ' + newTitle);
    }
}
