import { Injectable } from '@angular/core';
import { IpcService } from './ipc.service';

@Injectable()
export class PersistenceService {

    constructor(private ipc: IpcService) {
        this.ipc.send('InitNEDB');
    }

    insertRecibo(reciboData) {
        this.ipc.send('SaveRecibo', reciboData);
    }

}
