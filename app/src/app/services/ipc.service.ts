import { Injectable } from '@angular/core';
import { IpcRenderer, Remote } from 'electron';

@Injectable()
export class IpcService {
  private _ipc: IpcRenderer | undefined = void 0;
  private _remote: Remote | undefined = void 0;

  constructor() {
    if (window.require) {
      try {
        this._ipc = window.require('electron').ipcRenderer;
        this._remote = window.require('electron').remote;
      } catch (e) {
        throw e;
      }
    } else {
      console.warn('Electron\'s IPC was not loaded');
    }
  }

  public getRemote(): Remote {
    return this._remote;
  }

  public on(channel: string, listener: Function): void {
    if (!this._ipc) {
      console.warn('Electron\'s IPC was not loaded');
      return;
    }
    console.log('Listening to', channel, 'calls.');
    this._ipc.on(channel, listener);
  }

  public send(channel: string, ...args): void {
    if (!this._ipc) {
      console.warn('Electron\'s IPC was not loaded');
      return;
    }
    console.log('Sending', channel, 'calls.');
    this._ipc.send(channel, ...args);
  }

  public sendSync(channel: string, ...args): any {
    if (!this._ipc) {
      console.warn('Electron\'s IPC was not loaded');
      return;
    }
    console.log('Sending (sync)', channel, 'calls.');
    return this._ipc.sendSync(channel, ...args);
  }

}
