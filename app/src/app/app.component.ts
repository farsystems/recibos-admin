import { Component, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { IpcService } from './services';
import { ToasterConfig, ToasterService, Toast, BodyOutputType } from 'angular2-toaster';

import 'style-loader!angular2-toaster/toaster.css';

@Component({
  selector: 'ngx-app',
  template: '<toaster-container [toasterconfig]="config"></toaster-container><router-outlet></router-outlet>',
})
export class AppComponent {

  config: ToasterConfig;

  constructor(
    private ipc: IpcService,
    toasterService: ToasterService,
    private _zone: NgZone,
    private router: Router) {
    this.ipc.on('navigate', (event, routes) => {
      this._zone.run(() => {
        this.router.navigate(routes);
      });
    });


    this.config = new ToasterConfig({
      positionClass: 'toast-top-right',
      timeout: 5000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: false,
      animation: 'fade',
      limit: 5,
    });

    ipc.on('UpdateNotAvailable', () => {
      toasterService.pop('error', 'Nova versão', 'Nenhuma nova versão encontrada.');
    });

    ipc.on('CheckUpdate', () => {
      const toast: Toast = {
        type: 'info',
        title: 'Procurando atualização',
        body: 'Procurando por novas versões.',
        timeout: 10000,
        showCloseButton: true,
        // bodyOutputType: BodyOutputType.TrustedHtml,
      };
      toasterService.popAsync(toast);
      ipc.send('check-for-update'); 
    })
  }
}
