import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ListRecibosComponent } from './components/list.recibos.component';


const routes: Routes = [{
  path: '',
  component: ListRecibosComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RecibosRoutingModule {
}

export const routedComponents = [
  ListRecibosComponent,
];

