import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngx-modal',
  template: `
    <div class="modal-header">
    <span>{{ modalHeader }}</span>
      <button class="close" aria-label="Close" (click)="closeModal()">
        <span aria-hidden="true">X</span>
      </button>
    </div>
    <div class="modal-body">
      Você realmente deseja <strong>remover</strong> este recibo? <br /><br />
      Para confirmar, digite o CPF do pagador 
      <span><strong><mark>{{ limpaCPF(cpfPagador) }}</mark></strong></span> no campo abaixo. <br/><br/>
      <input class="form-control"
      style="padding: 0.375rem 0.45rem !important;"
       [(ngModel)]="confirmCPF"/>
    </div>
    <div class="modal-footer">
      <button class="btn btn-md btn-danger" (click)="performModal()">Remover</button>
      <button class="btn btn-md btn-secondary" (click)="closeModal()">Cancelar</button>
    </div>
  `,
  styles: [`
    mark {
      background-color: yellow;
      color: black;
      font-weight: bold;
    }
  `],
})
export class DeleteModalComponent {
  modalHeader: string;
  cpfPagador: String;
  confirmCPF: String;

  constructor(private activeModal: NgbActiveModal) { }

  performModal() {
    if(this.confirmCPF === this.limpaCPF(this.cpfPagador))
    this.activeModal.close(true);
  }

  closeModal() {
    this.activeModal.close(false);
  }

  limpaCPF(cpf: String): String {
    return cpf.replace(/\./g, '').replace('-', '');
  }
}
