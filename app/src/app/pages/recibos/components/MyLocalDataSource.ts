import { LocalDataSource } from 'ng2-smart-table';

export class MyLocalDataSource extends LocalDataSource {

    constructor(data: Array<any> = [], private sortCB: Function) {
        super(data);
    }

    setSort(conf: Array<any>, doEmit: boolean = true): LocalDataSource {
        if (doEmit) {
            this.sortCB(conf);
        }
        return super.setSort(conf, false);
    }

    isEmpty(): boolean {
        return this.count() <= 0;
    }
}
