import { Component, Injectable } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { NgbDateStruct, NgbCalendar, NgbDatepickerI18n, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import * as Mustache from 'mustache';

import { PersistenceService, IpcService, TitleService } from '../../../services';
import { MyLocalDataSource } from './MyLocalDataSource';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date';
import { DeleteModalComponent } from './delete-modal/modal.component';
import { Template } from '../../../templates/print_original';

const writtenNumber = require('written-number');

const I18N_VALUES = {
  'pt_BR': {
    weekdays: ['S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
    months: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho',
      'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
  },
};

const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
  one && two && two.year === one.year && two.month === one.month;

@Injectable()
export class I18n {
  language = 'pt_BR';
}

@Injectable()
export class CustomDatepickerI18n extends NgbDatepickerI18n {

  constructor(private _i18n: I18n) {
    super();
  }

  getWeekdayShortName(weekday: number): string {
    return I18N_VALUES[this._i18n.language].weekdays[weekday - 1];
  }
  getMonthShortName(month: number): string {
    return I18N_VALUES[this._i18n.language].months[month - 1];
  }
  getMonthFullName(month: number): string {
    return this.getMonthShortName(month);
  }
}

@Component({
  selector: 'ngx-recibos-list',
  styleUrls: ['./list.recibos.component.scss'],
  templateUrl: './list.recibos.component.html',
  providers: [I18n, { provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n }]
})
export class ListRecibosComponent {

  tipoBusca: String;
  anosSelect: Array<String>;

  hoveredDate: NgbDateStruct;
  fromDate: NgbDateStruct;
  fromDateView: String;
  nomePagador: String;
  nomePaciente: String;

  public recibos: MyLocalDataSource;
  public form: FormGroup;
  public isSearching: boolean = false;
  public settings;

  public totalItens;
  public paginaAtual = 1;
  public itensPorPagina = 10;
  public maxSize = 5;
  private sortData = {
    campo: 'emissao',
    direcao: 'desc',
  };

  private meses = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
    'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];

  constructor(
    private calendar: NgbCalendar,
    private fb: FormBuilder,
    private persist: PersistenceService,
    private ipc: IpcService,
    private titleService: TitleService,
    private modalService: NgbModal) {

    this.settings = {
      mode: 'external',
      hideSubHeader: true,
      pager: {
        display: false,
      },
      actions: {
        delete: true,
        add: false,
        edit: true,
        columnTitle: '',
        position: 'right',
      },
      edit: {
        editButtonContent: '<i class="fa fa-small fa-files-o tiny"></i>',
      },
      delete: {
        deleteButtonContent: '<i class="fa fa-small fa-times"></i>',
      },
      columns: {
        nome1: {
          title: 'Pagador',
          type: 'string',
        },
        cpf1: {
          title: 'CPF',
          type: 'string',
        },
        rg1: {
          title: 'RG',
          type: 'string',
        },
        nome2: {
          title: 'Paciente',
          type: 'string',
        },
        vencimento: {
          title: 'Data Vencimento',
          type: 'string',
        },
        tipo: {
          title: 'Tipo',
          type: 'string',
        },
        valor: {
          title: 'Valor',
          type: 'string',
        },
        emissao: {
          title: 'Data Emissão',
          type: 'string',
        },
      },
      noDataMessage: 'Nenhum registro encontrado',
    };

    this.tipoBusca = 'anual';

    this.buildAnosSelect();

    titleService.setTitle('Listagem de Recibos');
  }

  buildAnosSelect() {
    const anoInicio = 2018;
    const anoFim = new Date().getFullYear();
    this.anosSelect = new Array<String>();
    for (let i = anoFim; i >= anoInicio; i--) {
      this.anosSelect.push(i.toString());
    }
  }

  setFromDateView($event) {
    this.fromDateView = this.getDateView($event);
  }

  setFromYear($event) {
    const year = $event.target.value;
    if (year === 'null') {
      this.fromDate = null;
    } else {
      this.fromDate = { year: year, day: 1, month: 1 };
    }
  }

  getTipoBusca(): String {
    let tBusca = '';
    switch (this.tipoBusca) {
      case 'diaria':
        tBusca = 'Diária';
        break;
      case 'mensal':
        tBusca = 'Mensal';
        break;
      case 'anual':
        tBusca = 'Anual';
        break;
    }
    return tBusca;
  }

  doSearch() {
    this.isSearching = true;
    this.loadData();
  }

  paginado(event) {
    this.paginaAtual = event;
    this.loadData();
  }

  limparFiltros() {
    this.fromDate = null;
    this.fromDateView = null;
  }

  getDataFilter(): any {
    const date = this.fromDate;
    let ret;
    switch (this.tipoBusca) {
      case 'diaria':
        ret = {
          begin: new Date(date.year, date.month - 1, date.day, 0, 0, 0, 0),
          end: new Date(date.year, date.month - 1, date.day, 23, 59, 59, 999),
        };
        break;
      case 'mensal':
        date.day = 1;
        const dateEnd = this.calendar.getPrev(this.calendar.getNext(NgbDate.from(date), 'm', 1), 'd', 1);

        ret = {
          begin: new Date(date.year, date.month - 1, date.day, 0, 0, 0, 0),
          end: new Date(dateEnd.year, dateEnd.month - 1, dateEnd.day, 23, 59, 59, 999),
        };
        break;
      case 'anual':
        date.day = 1;
        date.month = 1;

        ret = {
          begin: new Date(date.year, 0, 1, 0, 0, 0, 0),
          end: new Date(date.year, 11, 31, 23, 59, 59, 999),
        };
        break;
    }

    return ret;
  }

  getDateView(date: NgbDateStruct): string {
    if (this.tipoBusca === 'diaria') {
      return date.day + '/' + this.meses[date.month - 1] + '/' + date.year;
    }
    if (this.tipoBusca === 'mensal') {
      return this.meses[date.month - 1] + '/' + date.year;
    }
    if (this.tipoBusca === 'anual') {
      return '' + date.year;
    }
  }

  loadData() {
    let sort = this.sortData.campo;
    if (this.sortData.direcao === 'desc') {
      sort = '-' + sort;
    }
    const criteria = {
      limit: this.itensPorPagina,
      skip: ((this.paginaAtual - 1) * this.itensPorPagina),
      sort: sort,
    };

    criteria['filter'] = {};
    if (this.fromDate) {
      criteria['filter'] = this.getDataFilter();
    }
    if (this.nomePaciente !== undefined && this.nomePaciente !== '') {
      criteria['filter']['paciente'] = this.nomePaciente
    }
    if (this.nomePagador !== undefined && this.nomePagador !== '') {
      criteria['filter']['pagador'] = this.nomePagador
    }

    const recibos = this.ipc.sendSync('GetRecibos', criteria);
    this.totalItens = recibos.count;
    recibos.data.forEach((p) => {
      p.vencimento = moment(p.vencimento).format('DD/MM/YYYY');
      p.emissao = moment(p.emissao).format('DD/MM/YYYY');
      p.tipo = (p.tipo === 'cheque' ? 'Cheque' : 'Dinheiro');
      if (p.centavos.toString().length <= 1) {
        p.centavos += '0';
      }
      p.valor = 'R$ ' + p.inteiro + ',' + p.centavos;
    });
    this.recibos = new MyLocalDataSource(recibos.data, (conf) => {
      this.sort(conf);
    });
  }

  doSort(event) {
  }

  private buildValues(recibos: any[]): void {
    const values: any = {};
    values.newRecibo = false;
    // Pagador
    values.cpf1 = recibos[0].cpf1;
    values.nome1 = recibos[0].nome1;
    values.data1 = recibos[0].datanasc1;
    values.rg1 = recibos[0].rg1;
    // Paciente
    values.cpf2 = recibos[0].cpf2;
    values.nome2 = recibos[0].nome2;
    values.data2 = recibos[0].datanasc2;
    let contParcela = -1;
    values.parcelas = [];
    for (let i = 0; i < recibos.length; i++) {
      values.parcelas[++contParcela] = {};
      values.parcelas[contParcela].tipo = recibos[i].tipo;
      values.parcelas[contParcela].vencimento = recibos[i].vencimento;
      values.parcelas[contParcela].inteiro = recibos[i].inteiro;
      if (parseInt(recibos[i].centavos, 10) > 0) {
        if (recibos[i].centavos.length < 2) {
          recibos[i].centavos += '0';
        }
        values.parcelas[contParcela].centavos = recibos[i].centavos;
      } else {
        values.parcelas[contParcela].centavos = '00';
      }

      values.parcelas[contParcela].texto = this.porExtenso(recibos[i].inteiro + '.' + recibos[i].centavos);
      const dVencimento = moment(recibos[i].vencimento).format('DD/MM/YYYY').split('/');

      values.parcelas[contParcela].dia = dVencimento[0];
      values.parcelas[contParcela].mes = this.getMes(parseInt(dVencimento[1], 10));
      values.parcelas[contParcela].ano = dVencimento[2];
      if (values.parcelas[contParcela].tipo === 'cheque') {
        values.parcelas[contParcela].numero = recibos[i].numero;
        values.parcelas[contParcela].banco = recibos[i].banco;
        values.parcelas[contParcela].agencia = recibos[i].agencia;
      }
    }
    return values;
  }

  private getMes(mes: number): string {
    return this.meses[mes - 1];
  }

  private porExtenso(valor): String {
    const val = valor.toString();
    const v = val.split('.');
    if (v[0] === '' || parseInt(v[0], 10) <= 0) {
      return '';
    }
    let ret = writtenNumber(v[0], { lang: 'pt' }) + ' reais';
    if (v.length > 1 && parseInt(v[1], 10) > 0) {
      if (v[1].length < 2) {
        v[1] += '0';
      }
      ret += ' e ' + writtenNumber(v[1], { lang: 'pt' }) + ' centavos';
    }

    return ret;
  }

  edit(event) {
    let grupo = event.data.grupo;
    let recibos = this.ipc.sendSync('GetReciboByGroup', grupo);
    const t = new Template();
    const computedValues = this.buildValues(recibos.data);
    const output = Mustache.render(t.data, computedValues);
    const file = 'data:text/html;charset=UTF-8,' + encodeURIComponent(output);
    this.ipc.send('loadRecibo', file, computedValues, this.ipc.getRemote().getCurrentWindow().id);
  }

  delete(event) {
    const activeModal = this.modalService.open(DeleteModalComponent, {
      size: 'sm',
      backdrop: 'static',
      container: 'nb-layout',
    });
    activeModal.componentInstance.modalHeader = 'Remover Recibo?';
    activeModal.componentInstance.cpfPagador = event.data.cpf1;

    activeModal.result.then((status) => {
      if(status === true) {
        this.ipc.sendSync("DeleteReciboByGroup", event.data.grupo);
        this.loadData();
      }
    });
  }

  private sort(conf) {
    this.sortData.campo = conf[0].field;
    this.sortData.direcao = conf[0].direction;
    this.loadData();
    this.recibos.setSort(conf, false);
  }

  isHovered(date) {
    return equals(date, this.hoveredDate);
  }

  isInside(date) {
    return equals(date, this.fromDate);
  }
}
