import { Component } from '@angular/core';

@Component({
  selector: 'ngx-recibos',
  template: `
  <router-outlet></router-outlet>
  `,
})
export class RecibosComponent {
}
