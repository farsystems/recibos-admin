import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { RecibosComponent } from './recibos.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RecibosRoutingModule, routedComponents } from './recibos-routing.module';
import { DeleteModalComponent } from './components/delete-modal/modal.component';

@NgModule({
  imports: [
    RecibosRoutingModule,
    ThemeModule,
    Ng2SmartTableModule,
    NgbModule,
  ],
  declarations: [
    RecibosComponent,
    DeleteModalComponent,
    ...routedComponents,
  ],
  entryComponents: [
    DeleteModalComponent,
  ],
})
export class RecibosModule { }
