import { Component } from '@angular/core';
import { IpcService } from '../services';
import { ToasterService, ToasterConfig, Toast, BodyOutputType } from 'angular2-toaster';

@Component({
  selector: 'ngx-pages',
  template: `
  <ngx-sample-layout>
    <router-outlet></router-outlet>
  </ngx-sample-layout>
  `,
})
export class PagesComponent {
}
