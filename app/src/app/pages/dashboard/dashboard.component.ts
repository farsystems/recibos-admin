import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, AbstractControl } from '@angular/forms';
import createNumberMask from 'text-mask-addons/dist/createNumberMask.js';
import * as Mustache from 'mustache';
import { Template } from '../../templates/print_original';
import { PersistenceService, IpcService, TitleService } from '../../services';
import { CPFValidator, DateValidator, MoneyValidator } from './validators';
import * as moment from 'moment';
import { Router } from '@angular/router';

const writtenNumber = require('written-number');
const uuidv4 = require('uuid/v4');

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent {

  public form: FormGroup;
  public submitted: boolean = false;
  public moneyMask = createNumberMask({
    prefix: '',
    thousandsSeparatorSymbol: '.',
    decimalSymbol: ',',
    allowDecimal: true,
  });
  public cpfMask = [/\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/];
  public dataMask = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
  public valorTexto;
  public addParcelaText;
  private meses = ['janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho',
    'agosto', 'setembro', 'outubro', 'novembro', 'dezembro'];

  private owner;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private persist: PersistenceService,
    private ipc: IpcService,
    private titleService: TitleService) {

      this.ipc.on('printSent', (event, ultimoReciboData, save) => {
        if (save) {
          const groupUUID = uuidv4();
          let i = 0;
          const emissao = new Date();
          ultimoReciboData.parcelas.forEach(parcela => {
            i++;
            const data: any = {};
            data.cpf1 = ultimoReciboData.cpf1;
            data.nome1 = ultimoReciboData.nome1;
            data.data1 = ultimoReciboData.data1;
            data.rg1 = ultimoReciboData.rg1;
            data.cpf2 = ultimoReciboData.cpf2;
            data.nome2 = ultimoReciboData.nome2;
            data.data2 = ultimoReciboData.data2;
            data.tipo = parcela.tipo;
            data.vencimento = parcela.vencimento;
            data.emissao = emissao;
            data.inteiro = Number.parseInt(parcela.inteiro);
            data.centavos = Number.parseInt(parcela.centavos);
            data.grupo = groupUUID;
            if (parcela.tipo === 'cheque') {
              data.numero = parcela.numero;
              data.banco = parcela.banco;
              data.agencia = parcela.agencia;
            }

            // só salva uma via do recibo
            if (i % 2 === 0) {
              this.persist.insertRecibo(data);
            }
          });
        }

        this.cancelar();
      });

    this.form = this.fb.group({
      '_id': [''],
      'cpf1': ['', Validators.compose([
        Validators.required,
        CPFValidator.validate])],
      'nome1': ['', Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(250)])],
      'rg1': ['', Validators.compose([
        Validators.required])],
      'datanasc1': ['', Validators.compose([
        Validators.required,
        DateValidator.validate])],
      'cpf2': ['', Validators.compose([
        Validators.required,
        CPFValidator.validate])],
      'nome2': ['', Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(250)])],
      'datanasc2': ['', Validators.compose([
        Validators.required,
        DateValidator.validate])],
      'valores': this.fb.array([this.initValores()]),
      'updatedAt': [''],
      'createdAt': [''],
    });

    this.addEventTipo();

    this.cancelar();

    this.titleService.setTitle('Digitar Recibo');

    this.owner = JSON.parse(this.ipc.sendSync('getApplicationOwnerData'));
  }

  private parseDate(data): Date {
    return moment(data, 'MM-DD-YYYY').toDate();
  }

  private initValores() {
    return this.fb.group({
      'valor': ['', Validators.compose([Validators.required, MoneyValidator.validate])],
      'vencimento': ['', Validators.compose([Validators.required, DateValidator.validate])],
      'numero': ['', Validators.compose([Validators.required])],
      'banco': ['', Validators.compose([Validators.required])],
      'agencia': ['', Validators.compose([Validators.required])],
      'tipo': ['dinheiro'],
    });
  }

  public onSubmit(values: any): void {
    this.submitted = true;
    if (this.form.valid) {
      const t = new Template();
      const computedValues = this.buildValues(values);
      const output = Mustache.render(t.data, computedValues);
      const file = 'data:text/html;charset=UTF-8,' + encodeURIComponent(output);
      this.ipc.send('loadRecibo', file, computedValues, this.ipc.getRemote().getCurrentWindow().id);

      this.submitted = false;
    }

  }

  private buildValues(v: any): void {
    const values: any = {};
    values.newRecibo = true;
    // Pagador
    values.cpf1 = v.cpf1;
    values.nome1 = v.nome1;
    values.data1 = v.datanasc1;
    values.rg1 = v.rg1;
    // Paciente
    values.cpf2 = v.cpf2;
    values.nome2 = v.nome2;
    values.data2 = v.datanasc2;
    let contParcela = -1;
    values.parcelas = [];
    for (let i = 0; i < v.valores.length; i++) {
      const valor = v.valores[i].valor.toString().split('.');
      values.parcelas[++contParcela] = {};
      values.parcelas[contParcela].tipo = v.valores[i].tipo;
      values.parcelas[contParcela].vencimento = v.valores[i].vencimento;
      values.parcelas[contParcela].inteiro = valor[0];
      if (valor.length > 1 && parseInt(valor[1], 10) > 0) {
        if (valor[1].length < 2) {
          valor[1] += '0';
        }
        values.parcelas[contParcela].centavos = valor[1];
      } else {
        values.parcelas[contParcela].centavos = '00';
      }
      values.parcelas[contParcela].texto = this.porExtenso(v.valores[i].valor);
      const dVencimento = v.valores[i].vencimento.split('/');

      values.parcelas[contParcela].dia = dVencimento[0];
      values.parcelas[contParcela].mes = this.getMes(parseInt(dVencimento[1], 10));
      values.parcelas[contParcela].ano = dVencimento[2];
      if (values.parcelas[contParcela].tipo === 'cheque') {
        values.parcelas[contParcela].numero = v.valores[i].numero;
        values.parcelas[contParcela].banco = v.valores[i].banco;
        values.parcelas[contParcela].agencia = v.valores[i].agencia;
      }
      // Segunda via
      values.parcelas[++contParcela] = {};
      values.parcelas[contParcela].tipo = v.valores[i].tipo;
      values.parcelas[contParcela].vencimento = v.valores[i].vencimento;
      values.parcelas[contParcela].inteiro = valor[0];
      if (valor.length > 1 && parseInt(valor[1], 10) > 0) {
        values.parcelas[contParcela].centavos = valor[1];
      } else {
        values.parcelas[contParcela].centavos = '00';
      }
      values.parcelas[contParcela].texto = this.porExtenso(v.valores[i].valor);
      values.parcelas[contParcela].dia = dVencimento[0];
      values.parcelas[contParcela].mes = this.getMes(parseInt(dVencimento[1], 10));
      values.parcelas[contParcela].ano = dVencimento[2];
      if (values.parcelas[contParcela].tipo === 'cheque') {
        values.parcelas[contParcela].numero = v.valores[i].numero;
        values.parcelas[contParcela].banco = v.valores[i].banco;
        values.parcelas[contParcela].agencia = v.valores[i].agencia;
      }
    }

    values.owner = this.owner;

    return values;
  }

  private getMes(mes: number): string {
    return this.meses[mes - 1];
  }

  private porExtenso(valor): String {
    const val = valor.toString();
    const v = val.split('.');
    if (v[0] === '' || parseInt(v[0], 10) <= 0) {
      return '';
    }
    let ret = writtenNumber(v[0], { lang: 'pt' }) + ' reais';
    if (v.length > 1 && parseInt(v[1], 10) > 0) {
      if (v[1].length < 2) {
        v[1] += '0';
      }
      ret += ' e ' + writtenNumber(v[1], { lang: 'pt' }) + ' centavos';
    }

    return ret;
  }

  getVencimentoToday(): string {
    const d = new Date();
    return (d.getDate() < 10 ? '0' + d.getDate() : d.getDate()) + '/'
      + (d.getMonth() < 10 ? '0'
        + (d.getMonth() + 1) : (d.getMonth() + 1))
      + '/'
      + d.getFullYear();
  }

  addParcela() {
    let data = this.initValores();
    (<FormArray>this.form.controls['valores']).push(data);
    this.addEventTipo();
  }

  addEventTipo() {
    const lastControl = (<FormArray>this.form.get('valores')).controls.length;
    const valorControl = (<FormArray>this.form.get('valores')).controls[lastControl-1];
    const tipoControl = valorControl.get('tipo');
    tipoControl.valueChanges.subscribe((data) => {
      if(data === 'cheque') {
        (<FormArray>this.form.get('valores')).controls[lastControl-1].get('numero').enable();
        (<FormArray>this.form.get('valores')).controls[lastControl-1].get('banco').enable();
        (<FormArray>this.form.get('valores')).controls[lastControl-1].get('agencia').enable();
      } else {
        (<FormArray>this.form.get('valores')).controls[lastControl-1].get('numero').disable();
        (<FormArray>this.form.get('valores')).controls[lastControl-1].get('banco').disable();
        (<FormArray>this.form.get('valores')).controls[lastControl-1].get('agencia').disable();
      }

    });
    tipoControl.setValue(tipoControl.value);
  }

  getValores(): AbstractControl[] {
    return (<FormArray>this.form.controls['valores']).controls;
  }

  removeParcela(i: number) {
    const control = <FormArray>this.form.controls['valores'];
    control.removeAt(i);
  }

  copiaPagador() {
    this.form.controls['cpf2'].setValue(this.form.controls['cpf1'].value);
    this.form.controls['cpf2'].markAsTouched();
    this.form.controls['nome2'].setValue(this.form.controls['nome1'].value);
    this.form.controls['nome2'].markAsTouched();
    this.form.controls['datanasc2'].setValue(this.form.controls['datanasc1'].value);
    this.form.controls['datanasc2'].markAsTouched();
  }

  public cancelar(): void {
    this.form.reset();
    this.submitted = false;
    this.valorTexto = '';
    for (let i = this.form.controls['valores']['controls'].length; i >= 1; i--) {
      this.removeParcela(i);
    }
    this.form.controls['valores']['controls'][0].controls.valor.setValue(0);
    this.form.controls['valores']['controls'][0].controls.vencimento.setValue(this.getVencimentoToday());
  }
}
