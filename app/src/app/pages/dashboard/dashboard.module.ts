import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { TextMaskModule } from 'angular2-text-mask';
import { ToasterModule } from 'angular2-toaster';

@NgModule({
  imports: [
    ThemeModule,
    CurrencyMaskModule,
    TextMaskModule,
    ToasterModule
  ],
  declarations: [
    DashboardComponent,
  ],
})
export class DashboardModule { }
