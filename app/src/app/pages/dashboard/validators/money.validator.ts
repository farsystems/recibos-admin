import { AbstractControl } from '@angular/forms';
import * as moment from 'moment';

export class MoneyValidator {

    public static validate(c: AbstractControl) {
        if (c.value !== null && MoneyValidator.testaValor(c.value.toString())) {
            return undefined;
        } else {
            return {
                maneyvalidator: {
                    valid: false,
                },
            };
        }
    }

    private static testaValor(data: string) {
        return Number.parseFloat(data) > 0;
    }
}
