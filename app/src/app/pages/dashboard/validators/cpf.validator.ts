import { AbstractControl } from '@angular/forms';

export class CPFValidator {

    public static validate(c: AbstractControl) {
        if (c.value !== null && CPFValidator.testaCPF(c.value.toString().replace(/\D/g, ''))) {
            return undefined;
        } else {
            return {
                cpfvalidator: {
                    valid: false,
                },
            };
        }
    }

    private static testaCPF(strCPF: string) {
        let soma = 0;
        let resto;
        if (strCPF.length !== 11)
            return false;
        if (strCPF === '00000000000')
            return false;

        for (let i = 1; i <= 9; i++)
            soma = soma + parseInt(strCPF.substring(i - 1, i), 10) * (11 - i);

        resto = (soma * 10) % 11;

        if (resto === 10 || resto === 11)
            resto = 0;
        if (resto !== parseInt(strCPF.substring(9, 10), 10))
            return false;

        soma = 0;

        for (let i = 1; i <= 10; i++)
            soma = soma + parseInt(strCPF.substring(i - 1, i), 10) * (12 - i);
        resto = (soma * 10) % 11;

        if ((resto === 10) || (resto === 11))
            resto = 0;
        if (resto !== parseInt(strCPF.substring(10, 11), 10))
            return false;

        return true;
    }
}
