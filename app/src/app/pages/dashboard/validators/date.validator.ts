import { AbstractControl } from '@angular/forms';
import * as moment from 'moment';

export class DateValidator {

    public static validate(c: AbstractControl) {
        if (c.value !== null && DateValidator.testaData(c.value.toString())) {
            return undefined;
        } else {
            return {
                datevalidator: {
                    valid: false,
                },
            };
        }
    }

    private static testaData(data: string) {
        return moment(data, 'DD/MM/YYYY', true).isValid();
    }
}
