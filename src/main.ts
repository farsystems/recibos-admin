// This is main process of Electron, started as first thing when your
// app starts. This script is running through entire life of your application.
// It doesn't have any windows which you can see on screen, but we can open
// window from here.

function start() {
    if (require('electron-squirrel-startup')) return;
}
start();

import { app, Menu, autoUpdater, BrowserWindow, ipcMain, dialog } from 'electron';
import devMenuTemplate from './helpers/menu_template';
import createWindow from './helpers/window';
import { IPCCallHandller } from './helpers/IPCCallHandller';
let client = require('electron-connect').client;

let isAutoUpdate = true;


// Special module holding environment variables which you declared
// in config/env_xxx.json file.
import env from './env';

const arch = require('os').arch();
const appVersion = require('../package.json').version;

let updateFeed = `https://dl.bintray.com/frieck/recibos${arch == 'x64' ? 'x64' : 'x86'}`

let mainWindow: BrowserWindow;

let menu: Menu;

let envData = env();



let setApplicationMenu = function (mw: BrowserWindow) {
    Menu.setApplicationMenu(Menu.buildFromTemplate(devMenuTemplate(mw)));
};

let isInternalDebugDisabled = function(): Boolean {
    let internalDebugDisabled = false;
    let args = process.argv;
    for (let i = 0; i < args.length; i++) {
        if (args[i] === '--disableInternalDebug') {
            internalDebugDisabled = true;
            break;
        }
    }
    return internalDebugDisabled;
};

app.on('ready', function () {
    mainWindow = createWindow('main', {
        width: 1000,
        height: 600,
        minWidth: 950,
        minHeight: 650
    });

    setApplicationMenu(mainWindow);
    let ipcHandller = new IPCCallHandller();

    mainWindow.loadURL(`file://${__dirname}/index.html`);

    if (envData.name !== 'production' &&  !isInternalDebugDisabled()) {
        mainWindow.webContents.openDevTools();
    }

    console.log(envData.name);

    if (envData.name === 'development') {
        client.create(mainWindow);
    }
});

app.on('window-all-closed', function () {
    app.quit();
});

autoUpdater.on('update-available', () => {
    console.log("Update is available!");
    if (!isAutoUpdate)
        mainWindow.webContents.send('UpdateAvailable');
});

autoUpdater.on('update-not-available', () => {
    console.log("Update not available!");
    if (!isAutoUpdate)
        mainWindow.webContents.send('UpdateNotAvailable');
})

autoUpdater.on('update-downloaded', (event, releaseNotes, releaseName, releaseDate, updateURL) => {
    var index = dialog.showMessageBox(mainWindow, {
        type: 'info',
        buttons: ['Reiniciar', 'Instalar depois'],
        title: "Gerenciador de Recibos",
        message: 'Uma nova versão foi encontrada!\nPor favor, reinicie a aplicação para instalar.'
    });

    if (index === 1) {
        return;
    }

    autoUpdater.quitAndInstall();
});

ipcMain.on('check-for-update', (event) => {
    isAutoUpdate = false;
    checkForUpdates();
})

setTimeout(function() {
    checkForUpdates()
}, 1000 * 60);

function checkForUpdates() {
    console.log("Checking for updates...");
    autoUpdater.setFeedURL(updateFeed + '?v=' + appVersion);
    autoUpdater.checkForUpdates();
}


