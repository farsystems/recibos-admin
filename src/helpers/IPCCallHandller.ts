import { ipcMain, BrowserWindow, app } from 'electron';
import { existsSync, mkdirSync } from 'fs';
import { NEDBFunctions, PrintFunctions, GeneralFunctions } from './ipc.functions';

export class IPCCallHandller {
    nedbFunctions: NEDBFunctions;
    printFunctions: PrintFunctions;
    generalFunctions: GeneralFunctions;
    constructor() {

        this.nedbFunctions = new NEDBFunctions(ipcMain);
        this.printFunctions = new PrintFunctions(ipcMain);
        this.generalFunctions = new GeneralFunctions(ipcMain);

    }

}
