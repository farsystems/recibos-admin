import { AbstractFunctions } from './abstract.functions';
import { app, IpcMain, BrowserWindow } from 'electron';
import { readFileSync } from 'fs';


export class GeneralFunctions extends AbstractFunctions {

  constructor(private ipcMain: IpcMain) {
    super(ipcMain);
    this.installFunctions();
  }

  installFunctions(): void {
    this.ipcMain.on('getApplicationPackage', (event, args) => {
      event.returnValue = readFileSync(__dirname + '/package.json', 'utf8');
    });

    this.ipcMain.on('getApplicationOwnerData', (event) => {
      let ownerFile = app.getPath('userData') + '/owner.json';
      event.returnValue = readFileSync(ownerFile, 'utf8');
    });
  }
}

