import { AbstractFunctions } from './abstract.functions';
import { app, IpcMain, BrowserWindow } from 'electron';


export class PrintFunctions extends AbstractFunctions {
  private printWin: BrowserWindow;
  private ultimoRecibo: string;
  private ultimoReciboData: any;
  private reciboWindow: any;

  constructor(private ipcMain: IpcMain) {
    super(ipcMain);
    this.installFunctions();
  }

  installFunctions(): void {
    this.ipcMain.on('printUltimoRecibo', (event, args) => {
      this.printWin = new BrowserWindow({ width: 900, height: 700 });
      this.printWin.loadURL(this.ultimoRecibo);
      this.printWin.show();
    });

    this.ipcMain.on('printRecibo', (event, save) => {
      this.printWin.webContents.print({}, (done) => {
        if (done) {
            BrowserWindow.fromId(this.reciboWindow).webContents.send('printSent', this.ultimoReciboData, save);
            this.printWin.hide();
        } else {
            console.log('Registro NAO impresso...');
        }
    });
    console.log('Imprimindo recibo...');
    
    });

    this.ipcMain.on('loadRecibo', (event, reciboUrl, reciboData, reciboWindow) => {
      this.ultimoRecibo = reciboUrl;
      this.ultimoReciboData = reciboData;
      this.printWin = new BrowserWindow({ width: 900, height: 700 });

      this.printWin.loadURL(this.ultimoRecibo);
      this.printWin.show();
      this.reciboWindow = reciboWindow;
  });
  }
}

