import { IpcMain } from 'electron';

export abstract class AbstractFunctions {
    constructor(ipcMain: IpcMain) {
    }

    abstract installFunctions(): void;
}
