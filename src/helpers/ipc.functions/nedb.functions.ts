
import { AbstractFunctions } from './abstract.functions';
import { app, IpcMain } from 'electron';
import { existsSync, mkdirSync } from 'fs';
import { connect } from 'camo';
import { Recibo } from '../models/recibo.model';
import * as moment from 'moment';

export class NEDBFunctions extends AbstractFunctions {
  private database;

  constructor(private ipcMain: IpcMain) {
    super(ipcMain);
    this.installFunctions();
  }

  installFunctions(): void {
    this.ipcMain.on('InitNEDB', () => {
      let dataDirectory = app.getPath('userData') + '/data';
      console.log(dataDirectory);
      if (!existsSync(dataDirectory)) {
        mkdirSync(dataDirectory);
      }

      let uri = 'nedb://' + dataDirectory;
      let self = this;
      connect(uri).then(function (db) {
        self.database = db;
      });
    });

    this.ipcMain.on('SaveRecibo', (evento, reciboData) => {
      reciboData.data1 = this.parseDate(reciboData.data1);
      reciboData.data2 = this.parseDate(reciboData.data2);
      reciboData.vencimento = this.parseDate(reciboData.vencimento);
      let recibo = Recibo.create(reciboData);
      recibo.save();
      console.log('Recibo salvo...');
    });

    this.ipcMain.on('GetRecibos', (evento, opt) => {
      let ret: any = {};
      console.log(opt);

      Recibo.count({}).then(count => {
        ret.count = count;
        let conf = { limit: opt.limit, skip: opt.skip, sort: opt.sort };
        let filter = {};
        if (opt.filter.begin !== undefined && opt.filter.begin !== undefined) {
          filter['$and'] = [
            { emissao: { $gte: new Date(opt.filter.begin) } },
            { emissao: { $lte: new Date(opt.filter.end) } },
          ];
        }
        if(opt.filter.pagador !== undefined) {
          filter['nome1'] = { $regex: new RegExp(opt.filter.pagador, 'i')};
        }
        if(opt.filter.paciente !== undefined) {
          filter['nome2'] = { $regex: new RegExp(opt.filter.paciente, 'i')};
        }
        Recibo.find(filter, conf).then(recibos => {
          ret.data = recibos;
          evento.returnValue = ret;
        });
      });
    });

    this.ipcMain.on('GetReciboByGroup', (evento, grupo) => {
      let ret: any = {};

      let filter = {grupo: grupo};
      Recibo.find(filter, {sort: 'vencimento'}).then(recibos => {
        ret.data = recibos;
        evento.returnValue = ret;
      });
    });

    this.ipcMain.on('DeleteReciboByGroup', (evento, grupo) => {
      let filter = {grupo: grupo};
      Recibo.deleteMany(filter).then(function(numDeleted) {
        evento.returnValue = numDeleted;
      });
    });
  }

  private parseDate(data): Date {
    return moment(data, 'DD/MM/YYYY').toDate();
  }
}
