import * as xl from 'excel4node';

export class ExcelGenerator {
    wb: any;
    ws: any;

    header: any;
    content: any;
    currentLine = 0;
    count = 0;

    constructor() {
        this.wb = new xl.Workbook();
        this.ws = this.wb.addWorksheet('Recibos');
        this.header = this.wb.createStyle({
            font: {
                bold: true,
                color: 'White',
            },
            alignment: {
                wrapText: true,
                horizontal: 'left'
            },
            fill: {
                type: 'pattern',
                patternType: 'solid',
                fgColor: 'Black',
                bgColor: 'Black',
            },
            border: { // §18.8.4 border (Border)
                left: {
                    style: 'thin',
                    color: '#444444'
                },
                right: {
                    style: 'thin',
                    color: '#444444'
                },
                top: {
                    style: 'thin',
                    color: '#444444'
                },
                bottom: {
                    style: 'thin',
                    color: '#444444'
                },
                diagonal: {
                    style: 'thin',
                    color: '#444444'
                },
                diagonalDown: true,
                outline: true
            },
        });

        this.content = this.wb.createStyle({
            font: {
                color: 'Black',
            },
            border: { // §18.8.4 border (Border)
                left: {
                    style: 'thin',
                    color: 'Black'
                },
                right: {
                    style: 'thin',
                    color: 'Black'
                },
                top: {
                    style: 'thin',
                    color: 'Black'
                },
                bottom: {
                    style: 'thin',
                    color: 'Black'
                },
                diagonal: {
                    style: 'thin',
                    color: 'Black'
                },
                diagonalDown: true,
                outline: true
            },
        });

        this.buildHeader();
    }

    buildHeader() {
        this.ws.cell(++this.currentLine, 1).string('#').style(this.header);
        this.ws.cell(this.currentLine, 2).string('CPF').style(this.header);
        this.ws.cell(this.currentLine, 3).string('Nome').style(this.header);
        this.ws.column(3).setWidth(50);
        this.ws.cell(this.currentLine, 4).string('Data').style(this.header);
        this.ws.cell(this.currentLine, 5).string('Valor').style(this.header);
    }

    buildData(data) {

    }
}
