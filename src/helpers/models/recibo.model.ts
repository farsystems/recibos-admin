import { Document as CamoDocument, DocumentSchema, SchemaTypeExtended } from 'camo';


export interface ReciboSchema extends DocumentSchema {
    // Pagador
    cpf1: String;
    nome1: String;
    data1: Date;
    rg1: String;
    // Paciente
    cpf2: String;
    nome2: String;
    data2: Date;
    emissao: Date;
    

    vencimento: Date;
    tipo: String;
    numero: String;
    banco: String;
    agencia: String;
    inteiro: Number;
    centavos: Number;
    grupo: String;
}

export class Recibo extends CamoDocument<ReciboSchema> {
    cpf1: SchemaTypeExtended = String;
    nome1: SchemaTypeExtended = String;
    data1: SchemaTypeExtended = Date;
    rg1: SchemaTypeExtended = String;
    cpf2: SchemaTypeExtended = String;
    nome2: SchemaTypeExtended = String;
    data2: SchemaTypeExtended = Date;
    emissao: SchemaTypeExtended = Date;

    vencimento: SchemaTypeExtended = Date;
    tipo: SchemaTypeExtended = String;
    numero: SchemaTypeExtended = String;
    banco: SchemaTypeExtended = String;
    agencia: SchemaTypeExtended = String;
    inteiro: SchemaTypeExtended = Number;
    centavos: SchemaTypeExtended = Number;


    grupo: SchemaTypeExtended = String;

    static collectionName() {
        return 'recibos';
    }
}
