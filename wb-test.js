let xl = require('excel4node');

var wb = new xl.Workbook();
var ws = wb.addWorksheet('Recibos');
var header = wb.createStyle({
    font: {
        bold: true,
        color: 'White',
    },
    alignment: {
        wrapText: true,
        horizontal: 'left'
    },
    fill: {
        type: 'pattern',
        patternType: 'solid',
        fgColor: 'Black',
        bgColor: 'Black',
    },
    border: { // §18.8.4 border (Border)
        left: {
            style: 'thin',
            color: '#444444'
        },
        right: {
            style: 'thin',
            color: '#444444'
        },
        top: {
            style: 'thin',
            color: '#444444'
        },
        bottom: {
            style: 'thin',
            color: '#444444'
        },
        diagonal: {
            style: 'thin',
            color: '#444444'
        },
        diagonalDown: true,
        outline: true
    },
});

let currentLine = 0;
let count = 0;

var content = wb.createStyle({
    font: {
        color: 'Black',
    },

    border: { // §18.8.4 border (Border)
        left: {
            style: 'thin',
            color: 'Black'
        },
        right: {
            style: 'thin',
            color: 'Black'
        },
        top: {
            style: 'thin',
            color: 'Black'
        },
        bottom: {
            style: 'thin',
            color: 'Black'
        },
        diagonal: {
            style: 'thin',
            color: 'Black'
        },
        diagonalDown: true,
        outline: true
    },
});

let buildHeader = () => {
    ws.cell(++currentLine, 1).string('#').style(header);
    ws.cell(currentLine, 2).string('CPF').style(header);
    ws.cell(currentLine, 3).string('Nome').style(header);
    ws.column(3).setWidth(50);
    ws.cell(currentLine, 4).string('Data').style(header);
    ws.cell(currentLine, 5).string('Valor').style(header);

}

let buildContent = () => {
    ws.cell(++currentLine, 1).number(++count).style(content);
    ws.cell(currentLine, 2).string('').style(content);
    ws.cell(currentLine, 3).string('Felipe Augusto Rieck').style(content);
    ws.cell(currentLine, 4).date(new Date()).style({ numberFormat: 'dd/mm/yyyy' }).style(content);
    ws.cell(currentLine, 5).string('').style(content);
}

buildHeader();
buildContent();
wb.write('ExcelFile.xlsx');